#ifndef CONFIG_H
#define CONFIG_H

#include <QtWidgets>

class Config
{
public:
    Config();
    int loadEvents(QStringList *eventList);
    int saveEvents(QStringList *eventList);
    int saveDefaultEvents();
private:
    QFile *eventFile;
};

#endif // CONFIG_H
