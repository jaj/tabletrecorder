#include "database.h"

Database::Database() {
    settings = new QSettings;
    fsyr = NULL;
    fani = NULL;
}

void Database::open() {
    if (settings->value("databasefolder").toString() == "")
        settings->setValue("databasefolder", QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation) + "/patientdata/");
    datadir = new QDir(settings->value("databasefolder").toString());
    if (!datadir->exists()) {
        qWarning("Creating database directory");
        if (!datadir->mkpath(settings->value("databasefolder").toString()))
            qWarning("Failed to create directory");
    }
}

void Database::newInterv()
{
    if (fsyr && fsyr->isOpen())
        fsyr->close();
    if (fani && fani->isOpen())
        fani->close();
    patientname = new QString("unknown");
    timestarted = new QString(QDateTime::currentDateTime().toString("yyyy-MM-dd_HHmmss.zzz"));
    if (!datadir->exists(*patientname + "/" + *timestarted))
        if (!datadir->mkpath(*patientname + "/" + *timestarted))
            qWarning("Failed to create directory");
    fsyr = new QFile(datadir->absoluteFilePath(*patientname + "/" + *timestarted + "/seringues.txt"));
    if (fsyr->open(QIODevice::ReadWrite)) {
        QTextStream dsyr(fsyr);
        dsyr << "temps,syr1,syr2,syr3,syr4,syr5,syr6\n";
    }
    fani = new QFile(datadir->absoluteFilePath(*patientname + "/" + *timestarted + "/ani.txt"));
    if (fani->open(QIODevice::ReadWrite)) {
        QTextStream dani(fani);
        dani << "temps,evenement\n";
    }
}

void Database::setPatientName(QString name)
{
    if (fsyr && fsyr->isOpen())
        fsyr->close();
    if (fani && fani->isOpen())
        fani->close();
    if (!datadir->mkdir(name))
        qWarning("Failed to create directory");
    if (!datadir->rename(*patientname + "/" + *timestarted, name + "/" + *timestarted))
        qWarning("Failed to rename directory");
    // delete previous patientname dir if empty
    *patientname = name;
    fsyr = new QFile(datadir->absoluteFilePath(*patientname + "/" + *timestarted + "/seringues.txt"));
    fsyr->open(QIODevice::ReadWrite | QIODevice::Append);
    fani = new QFile(datadir->absoluteFilePath(*patientname + "/" + *timestarted + "/ani.txt"));
    fani->open(QIODevice::ReadWrite | QIODevice::Append);
}

void Database::writeEvents(QString ev)
{
    QFile f(datadir->absoluteFilePath(*patientname + "/" + *timestarted + "/evenements.txt"));
    if (f.open(QFile::WriteOnly | QFile::Truncate)) {
        QTextStream data(&f);
        data << ev.toLatin1().data();
    }
    f.close();
}

void Database::appendSyr(QString syr)
{
    QTextStream dsyr(fsyr);
    dsyr << syr.toLatin1().data();
}

void Database::headerSyr(QString hdr)
{
    QString newsyr;
    fsyr->seek(0);
    QTextStream dsyr(fsyr);
    dsyr.readLine();
    newsyr.append(hdr);
    newsyr.append(dsyr.readAll());
    fsyr->resize(0);
    dsyr << newsyr;
}

void Database::appendAni(QString ani)
{
    QTextStream dani(fani);
    dani << ani.toLatin1().data();
}

