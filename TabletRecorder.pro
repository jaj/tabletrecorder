#-------------------------------------------------
#
# Project created by QtCreator 2018-02-25T19:02:09
#
#-------------------------------------------------

QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
static {
    QTPLUGIN += qtvirtualkeyboardplugin
}

TARGET = TabletRecorder
TEMPLATE = app

CONFIG += static

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

#LIBS += -ladvapi32
#LIBS += ADVapi32.Lib

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    config.cpp \
    database.cpp

win32: SOURCES +=
	win32/ani.cpp
	win32/fresenius.cpp

HEADERS += \
    mainwindow.h \
    config.h \
    database.h

win32: SOURCES +=
	win32/ani.h
	win32/fresenius.h

linux: INCLUDEPATH += /usr/lib/gcc/x86_64-linux-gnu/8/include
linux: INCLUDEPATH += /usr/lib/gcc/x86_64-linux-gnu/8/include-fixed

win32: LIBS += C:/Program Files (x86)/Microsoft SDKs/Windows/v7.1A/Lib/AdvAPI32.Lib
win32: LIBS += -L$$PWD/'../../../../Program Files (x86)/Microsoft SDKs/Windows/v7.1A/Lib/x64/' -lAdvAPI32 -lunicows

