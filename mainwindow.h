#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtWidgets>
//#include <QSerialPortInfo>

#include <ctime>
#include "config.h"

#ifdef _WIN32
#include "fresenius.h"
#include "ani.h"
#endif
#include "database.h"

#define STRSIZE 4096

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    MainWindow(QWidget *parent = NULL);
    ~MainWindow();
private slots:
    void update();
    void updateClock();
    void insertQuickEvent();
    void delEvent();
    void insertEventPanel();
    void addEvent();
    void dumpEvents();
    void put();
    void cancelEvent();
    void addButton(QString, QString, int x, int y);
    void modPatient();
    void finPatient();
    void newCaseTimeout();
    void pushSyrHdr();
private:
    QTimer *t;
    std::time_t t_anilast;
    std::time_t t_freselast;
    struct timeval t_lastevent;
    QString lastevent;
    Database *db;

    QVBoxLayout *serialLayout;
    QVBoxLayout *aniLayout;
    QStackedLayout *eventsLayout;
    QTableWidget *eventsTableWidget;
    QLineEdit *syrName[6];

    QLabel *clkWidget;
    QLabel *syrLabel[6];
    QLabel *aniLabel;
    QLabel *volNameLabel;
    QList<QPushButton*> *eventButtonList;
    QLineEdit *eventLineEdit;
    QStringList *eventList;
    QPushButton *caseButton;
    QGridLayout *quickEventsLayout;
    QLineEdit *nameLineEdit;
    QLineEdit *nipLineEdit;
    QPushButton *modPatientBut;
    QPushButton *finPatientBut;
    
    QTimer *newCaseTimer;

    #ifdef _WIN32
    Fresenius *fres;
    Ani *ani;
    #endif
};

#endif // MAINWINDOW_H

