#include "mainwindow.h"
#include "database.h"

#include <sys/time.h>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    /* Database */
    db = new Database();
    db->open();
    db->newInterv();

    /* Main Window */
    setWindowTitle(tr("TabletRecorder"));
    QSplitter *mainWidget = new QSplitter;
    QVBoxLayout *leftLayout = new QVBoxLayout;
    QVBoxLayout *rightLayout = new QVBoxLayout;
    QWidget *leftWidget = new QWidget;
    QWidget *rightWidget = new QWidget;
    leftWidget->setLayout(leftLayout);
    rightWidget->setLayout(rightLayout);
    mainWidget->addWidget(leftWidget);
    mainWidget->addWidget(rightWidget);
    mainWidget->setStyleSheet("QGroupBox { font-family: \"Segoe UI Semibold\"; font-size: 20px; } "
        "QLabel { font-family: \"Segoe UI SemiLight\"; font-size: 20px; } "
        "QHeaderView { font-family: \"Segoe UI SemiLight\"; font-size: 20px; } "
        "QTableView { font-family: \"Segoe UI SemiLight\"; font-size: 20px; } "
        "QPushButton { font-family: \"Segoe UI SemiLight\"; font-size: 20px; } "
        "QLineEdit { font-family: \"Segoe UI SemiLight\"; font-size: 20px; }");
    setCentralWidget(mainWidget);

    /* Clock */
    clkWidget = new QLabel;
    clkWidget->setAlignment(Qt::AlignCenter);
    clkWidget->setStyleSheet("font-size: 50px; color: rgb(50,50,50);");

    /* Patient */
    QGroupBox *patientGroup = new QGroupBox("Patient");
    QVBoxLayout *patientLayout = new QVBoxLayout;
    patientGroup->setLayout(patientLayout);
    nameLineEdit = new QLineEdit();
    nameLineEdit->setPlaceholderText("Nom");
    nipLineEdit = new QLineEdit();
    nipLineEdit->setPlaceholderText("NIP");
    patientLayout->addWidget(nameLineEdit);
    patientLayout->addWidget(nipLineEdit);
    finPatientBut = new QPushButton("Fin Intervention");
    patientLayout->addWidget(finPatientBut);
    connect(nipLineEdit, SIGNAL(editingFinished()), this, SLOT(modPatient()));
    connect(nameLineEdit, SIGNAL(editingFinished()), this, SLOT(modPatient()));
    connect(finPatientBut, SIGNAL(released()), this, SLOT(finPatient()));

    /* Syringes */
    QGroupBox *syringeGroup = new QGroupBox("Fresenius Orchestra");
    QGridLayout *syringeLayout = new QGridLayout;
    syringeGroup->setLayout(syringeLayout);

    for (int i = 5; i >= 0; --i) {
        syrLabel[i] = new QLabel(tr(""));
        syrLabel[i]->setStyleSheet("QLabel{ padding: 0px 3px 3px 0px; }");
        syrLabel[i]->hide();
        syrName[i] = new QLineEdit("");
        connect(syrName[i], SIGNAL(editingFinished()), this, SLOT(pushSyrHdr()));
        syrName[i]->hide();
        syringeLayout->addWidget(syrLabel[i], 5-i, 0);
        syringeLayout->addWidget(syrName[i], 5-i, 1);
    }
    syrLabel[0]->setText("Pas de données");
    syrLabel[0]->show();
    QHBoxLayout *drugsLayout = new QHBoxLayout;
    
    //drugsLayout->addWidget();
    // hide layout

    /* ANI */
    QGroupBox *aniGroup = new QGroupBox("MDoloris ANI");
    aniLayout = new QVBoxLayout;
    aniGroup->setLayout(aniLayout);

    aniLabel = new QLabel(tr("Pas de données"));
    aniLabel->setStyleSheet("padding: 0px 3px 3px 0px;");
    aniLayout->addWidget(aniLabel);

    /* Left Panel */
    leftLayout->addWidget(clkWidget);
    leftLayout->addWidget(patientGroup);
    leftLayout->addWidget(syringeGroup);
    leftLayout->addWidget(aniGroup);
    leftLayout->addStretch(1);

    /* Events */
    QGroupBox *eventsGroup = new QGroupBox("Evenements");
    eventsLayout = new QStackedLayout;
    QWidget *listEventsWidget = new QWidget;
    QWidget *addEventsWidget = new QWidget;
    eventsLayout->addWidget(listEventsWidget);
    eventsLayout->addWidget(addEventsWidget);
    eventsLayout->setCurrentIndex(0);
    eventsGroup->setLayout(eventsLayout);

    /* List events */
    QVBoxLayout *listEventsLayout = new QVBoxLayout;
    listEventsWidget->setLayout(listEventsLayout);

    eventsTableWidget = new QTableWidget;
    eventsTableWidget->setRowCount(0);
    eventsTableWidget->setColumnCount(2);
    eventsTableWidget->setHorizontalHeaderLabels(QStringList() << "Heure" << "Evenement");
    eventsTableWidget->verticalHeader()->hide();
    eventsTableWidget->horizontalHeader()->setStretchLastSection(true);
    eventsTableWidget->horizontalHeader()->setHighlightSections(false);
    eventsTableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    eventsTableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
    eventsTableWidget->setSelectionMode(QAbstractItemView::SingleSelection);
    eventsTableWidget->sortByColumn(0, Qt::AscendingOrder);
    eventsTableWidget->setSortingEnabled(true);
    connect(eventsTableWidget->model(), SIGNAL(dataChanged(const QModelIndex&, const QModelIndex&)), this, SLOT(dumpEvents()));
    listEventsLayout->addWidget(eventsTableWidget);

    QHBoxLayout *eventsButLayout = new QHBoxLayout;
    listEventsLayout->addLayout(eventsButLayout);

    QPushButton *delEventBut = new QPushButton("Supprimer");
    QPushButton *addEventBut = new QPushButton("Ajouter");
    delEventBut->setStyleSheet("padding: 5px 20px 5px 20px; margin: 5px 0px 0px 0px;");
    addEventBut->setStyleSheet("padding: 5px 20px 5px 20px; margin: 5px 0px 0px 0px;");
    connect(delEventBut, SIGNAL(released()), this, SLOT(delEvent()));
    connect(addEventBut, SIGNAL(released()), this, SLOT(insertEventPanel()));
    eventsButLayout->addWidget(delEventBut);
    eventsButLayout->addWidget(addEventBut);

    /* Add events */
    QVBoxLayout *addEventsLayout = new QVBoxLayout;
    addEventsWidget->setLayout(addEventsLayout);

    eventLineEdit = new QLineEdit("");

    QHBoxLayout *numberLayout = new QHBoxLayout;
    QPushButton *b;
    for (QString s : {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"}) {
        b = new QPushButton(s);
        b->setObjectName(s);
        b->setStyleSheet("padding: 4px 0px 4px 0px;");
        numberLayout->addWidget(b);
        connect(b, SIGNAL(released()), this, SLOT(put()));
    }
    QHBoxLayout *unitLayout = new QHBoxLayout;
    for (QString s : {"pg", "ng", "ug", "mg", "g", "mL", "L", "mUI", "UI", "<<"}) {
        b = new QPushButton(s);
        b->setObjectName(s);
        b->setStyleSheet("padding: 4px 0px 4px 0px;");
        unitLayout->addWidget(b);
        connect(b, SIGNAL(released()), this, SLOT(put()));
    }
    quickEventsLayout = new QGridLayout();
    eventButtonList = new QList<QPushButton*>;
    addButton("Oxygenation", "", 0, 0);
    addButton("Ventilation Manuelle", "", 1, 0);
    addButton("Laryngoscopie", "", 2, 0);
    addButton("Intubation", "", 3, 0);
    addButton("Ventilation Mecanique", "", 4, 0);
    addButton("Incision", "", 5, 0);
    addButton("Remplissage", "", 6, 0);
    addButton("Extubation", "", 7, 0);
    addButton("", "", 8, 0);
    addButton("Propofol", "background-color: rgb(255,255,0);", 0, 1);
    addButton("Ketamine", "background-color: rgb(255,255,0);", 1, 1);
    addButton("Midazolam", "background-color: rgb(255,102,0);", 2, 1);
    addButton("Tracrium", "background-color: rgb(245,64,41);", 3, 1);
    addButton("Celocurine", "color: rgb(245,64,41); background-color: rgb(40,40,40);", 4, 1);
    addButton("Noradrenaline", "background-color: rgb(222,191,217);", 5, 1);
    addButton("Ephedrine", "background-color: rgb(222,191,217);", 6, 1);
    addButton("Adrenaline", "color: rgb(222,191,217); background-color: rgb(40,40,40);", 7, 1);
    addButton("Atropine", "background-color: rgb(163,217,99);", 8, 1);
    addButton("Ultiva", "background-color: rgb(133,199,227);", 0, 2);
    addButton("Morphine", "background-color: rgb(133,199,227);", 1, 2);
    addButton("Sufentanil", "background-color: rgb(133,199,227);", 2, 2);
    addButton("Droleptan", "background-color: rgb(237,194,130);", 3, 2);
    addButton("Perfalgan", "background-color: rgb(255,255,255);", 4, 2);
    addButton("Tramadol", "background-color: rgb(255,255,255);", 5, 2);
    addButton("Profenid", "background-color: rgb(255,255,255);", 6, 2);
    addButton("Acupan", "background-color: rgb(255,255,255);", 7, 2);
    addButton("Dexamethasone", "background-color: rgb(255,255,255);", 8, 2);
    
    QHBoxLayout *addEventActionLayout = new QHBoxLayout();
    QPushButton *return_event = new QPushButton("Annuler");
    QPushButton *confirm_event = new QPushButton("Ajouter");
    return_event->setStyleSheet("padding: 5px 20px 5px 20px; margin: 5px 0px 0px 0px;");
    confirm_event->setStyleSheet("padding: 5px 20px 5px 20px; margin: 5px 0px 0px 0px;");
    connect(return_event, SIGNAL(released()), this, SLOT(cancelEvent()));
    connect(confirm_event, SIGNAL(released()), this, SLOT(addEvent()));
    addEventActionLayout->addWidget(return_event);
    addEventActionLayout->addWidget(confirm_event);

    addEventsLayout->addWidget(eventLineEdit);
    addEventsLayout->addLayout(numberLayout);
    addEventsLayout->addLayout(unitLayout);
    addEventsLayout->addLayout(quickEventsLayout);
    addEventsLayout->addLayout(addEventActionLayout);
    
    rightLayout->addWidget(eventsGroup);
    rightLayout->addStretch(1);

    /* Start threads and timers */
#ifdef _WIN32
    fres = new Fresenius;
    fres->start();

    ani = new Ani;
    ani->start();
#endif

    t = new QTimer(this);
    connect(t, SIGNAL(timeout()), this, SLOT(update()));
    t->start(100);

    newCaseTimer = new QTimer(this);
    
    updateClock();
    QTimer *clktimer = new QTimer(this);
    connect(clktimer, SIGNAL(timeout()), this, SLOT(updateClock()));
    clktimer->start(1000);
}

MainWindow::~MainWindow()
{
    ;
}

void MainWindow::addButton(QString name, QString ss, int x, int y)
{
    QPushButton *b = new QPushButton(name);
    b->setObjectName(name);
    b->setStyleSheet("padding: 8px 0px 8px 0px; " + ss);
    quickEventsLayout->addWidget(b, x, y);
    connect(b, SIGNAL(released()), this, SLOT(insertQuickEvent()));
    b->setCheckable(true);
    eventButtonList->append(b);
}

void MainWindow::modPatient()
{
    if (nipLineEdit->text() == "" && nameLineEdit->text() == "") {
        db->setPatientName("unknown");
        return;
    }
    db->setPatientName(nipLineEdit->text() + "_" + nameLineEdit->text());
    QPalette *pal = new QPalette();
    pal->setColor(QPalette::Base, Qt::lightGray);
    pal->setColor(QPalette::Text, Qt::black);
    if (nameLineEdit->text() != "")
        nameLineEdit->setPalette(*pal);
    if (nipLineEdit->text() != "")
        nipLineEdit->setPalette(*pal);
}

void MainWindow::finPatient()
{
    if (nipLineEdit->text() != "" || nameLineEdit->text() != "")
        db->setPatientName(nipLineEdit->text() + "_" + nameLineEdit->text());
    db->newInterv();
    QPalette *pal = new QPalette();
    nameLineEdit->setReadOnly(true);
    nipLineEdit->setReadOnly(true);
    pal->setColor(QPalette::Base, Qt::lightGray);
    pal->setColor(QPalette::Text, Qt::black);
    nameLineEdit->setPalette(*pal);
    nipLineEdit->setPalette(*pal);
    finPatientBut->setText("Enregistrement...");
    newCaseTimer->singleShot(2000, this, SLOT(newCaseTimeout()));
}

void MainWindow::newCaseTimeout()
{
    finPatientBut->setText("Fin Intervention");
    nameLineEdit->setText("");
    nipLineEdit->setText("");
    eventsTableWidget->clearContents();
    eventsTableWidget->setRowCount(0);
    QPalette *pal = new QPalette();
    pal->setColor(QPalette::Base, Qt::white);
    pal->setColor(QPalette::Text, Qt::black);
    nameLineEdit->setPalette(*pal);
    nipLineEdit->setPalette(*pal);
    for (int i = 0; i < 6; ++i) {
        syrName[i]->clear();
        syrLabel[i]->hide();
        syrName[i]->hide();
    }
    syrLabel[0]->show();
    syrLabel[0]->setText("Pas de données");
    nameLineEdit->setReadOnly(false);
    nipLineEdit->setReadOnly(false);
}

void MainWindow::pushSyrHdr()
{
    QString hdr;

    hdr.append(syrName[0]->text());
    for (int i = 1; i < 6; ++i) {
        hdr.append(",");
        hdr.append(syrName[i]->text());
    }
    hdr.append("\n");
    db->headerSyr(hdr);
}

void MainWindow::put()
{
    QString a = sender()->objectName();
    if (a == "<<")
        eventLineEdit->backspace();
    else
        eventLineEdit->insert(a);
}

void MainWindow::updateClock()
{
    time_t rawtime;
    struct tm *timeinfo;
    char clkStr[9];
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    strftime(clkStr, 9, "%H:%M:%S", timeinfo);
    clkWidget->setText(clkStr);
}

void MainWindow::insertEventPanel()
{
    eventsLayout->setCurrentIndex(1);
}

void MainWindow::dumpEvents()
{
    QString ev;
    for (int row = 0; row < eventsTableWidget->model()->rowCount(); row++) {
        ev.append(eventsTableWidget->model()->data(eventsTableWidget->model()->index(row, 0)).toString());
        ev.append(",");
        ev.append(eventsTableWidget->model()->data(eventsTableWidget->model()->index(row, 1)).toString());
        ev.append("\n");
    }
    db->writeEvents(ev);
}

void MainWindow::cancelEvent()
{
    eventLineEdit->clear();
    eventsLayout->setCurrentIndex(0);
}

void MainWindow::addEvent()
{
    QPushButton *b = NULL;
    for (QList<QPushButton*>::iterator it = eventButtonList->begin(); it != eventButtonList->end(); ++it) {
        QPushButton* current = *it;
        if (current->isChecked()) {
            current->setChecked(false);
            b = current;
        }
    }
    eventsTableWidget->setSortingEnabled(false);
    int r = eventsTableWidget->rowCount();
    eventsTableWidget->insertRow(r);
    eventsTableWidget->setItem(r, 0, new QTableWidgetItem(QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss.zzz")));
    if (b)
        eventsTableWidget->setItem(r, 1, new QTableWidgetItem(b->text() + " " + eventLineEdit->text()));
    else
        eventsTableWidget->setItem(r, 1, new QTableWidgetItem(eventLineEdit->text()));
    eventsTableWidget->setSortingEnabled(true);
    eventLineEdit->clear();
    eventsLayout->setCurrentIndex(0);
}

void MainWindow::delEvent()
{
    if (eventsTableWidget->selectionModel()->hasSelection())
        eventsTableWidget->removeRow(eventsTableWidget->selectionModel()->selectedRows()[0].row());    
}

void MainWindow::insertQuickEvent()
{
    for (QList<QPushButton*>::iterator it = eventButtonList->begin(); it != eventButtonList->end(); ++it) {
        QPushButton* current = *it;
        if (current->text() != sender()->objectName())
            current->setChecked(false);
    }
}

void MainWindow::update()
{
#ifdef _WIN32
    struct vol_t t;
    struct ani_t ta;
    QString line, volumestr, st;
    unsigned int i;

    if (!fres->isrunning()) {
        for (i = 1; i < 6; ++i) {
            syrLabel[i]->hide();
            syrName[i]->hide();
        }
        syrLabel[0]->setText("Pas de données");
    }

    while (!fres->get_volume(&t)) {
        line.clear();
        line.append(QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss.zzz"));
        for (i = 0; i < t.syringe; ++i)
            line.append(',');
        volumestr.clear();
        volumestr.setNum((double) t.volume / 1000, 'f', 3);
        line.append(volumestr);
        for (i = 0; i < 6 - t.syringe; ++i)
            line.append(',');
        line.append("\n");
        db->appendSyr(line);
        syrLabel[t.syringe - 1]->setText(tr("%1 : %2 mL").arg(t.syringe).arg(volumestr));
        syrLabel[t.syringe - 1]->show();
        syrName[t.syringe - 1]->show();
        syrLabel[t.syringe - 1]->setStyleSheet("QLabel{ padding: 0px 3px 3px 0px; font-family: \"Segoe UI SemiLight\"; font-size: 24px; }");
        t_freselast = std::time(nullptr);
    }
    
    while (!ani->get_data(&ta)) {
        line.clear();
        line.append(QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss.zzz,"));
        line.append((char*)ta.ani_time);
        line.append(',');
        st.clear();
        st.setNum(ta.ani_running);
        line.append(st);
        line.append(',');
        st.clear();
        st.setNum(ta.ani_short);
        line.append(st);
        line.append(',');
        st.clear();
        st.setNum(ta.ani_long);
        line.append(st);
        line.append(',');
        st.clear();
        st.setNum((double) ta.ani_energy / 1000, 'f', 2);
        line.append(st);
        line.append("\n");
        db->appendAni(line);
        aniLabel->setText(tr("ANI %1 / %2").arg(ta.ani_short).arg(ta.ani_long));
        t_anilast = std::time(nullptr);
    }
    
    if (std::difftime(std::time(nullptr), t_anilast) > 10)
        aniLabel->setText("Pas de données");
#endif
}

