#include "mainwindow.h"

#ifdef _WIN32
#include "fresenius.h"
#include "ani.h"
#endif

//#define TABLET

#include <QApplication>

int main(int argc, char *argv[])
{
    //qputenv("QT_IM_MODULE", QByteArray("qtvirtualkeyboard"));

    QApplication a(argc, argv);
    a.setOrganizationName("LeTemple");
    a.setApplicationName("TabletRecorder");
    a.setStyle(QStyleFactory::create("Fusion"));

    MainWindow w;
    
#ifdef TABLET
    w.showFullScreen();
#else
    w.showMaximized();
#endif

    return a.exec();
}

