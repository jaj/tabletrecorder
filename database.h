#ifndef DATABASE_H
#define DATABASE_H

#include <QtCore>
#include <QDir>
#include <QSettings>

#include <sys/time.h>

class Database
{
public:
    Database();
    void open();
    void newInterv();
    void writeEvents(QString ev);
    void setPatientName(QString name);
    void appendSyr(QString syr);
    void headerSyr(QString hdr);
    void appendAni(QString ani);
private:
    QDir *datadir;
    QSettings *settings;
    QString *timestarted;
    QString *patientname;
    QFile *fsyr;
    QFile *fani;
};

#endif // DATABASE_H