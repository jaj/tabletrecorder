#include "serial.h"

Serial::Serial()
{
;
}

int Serial::listSerial()
{
    int i, r;
    HKEY k;
    TCHAR classname[MAX_PATH] = TEXT("");
    DWORD classnamesize = MAX_PATH, subkeys = 0, maxsubkey, maxclass, nvalues, maxvalue, maxvaluedata, secdescr;
    TCHAR name[BLEN];
    BYTE data[BLEN];
    DWORD name_len, data_len, type;
    FILETIME lastwrite;

    if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, TEXT("HARDWARE\\DEVICEMAP\\SERIALCOMM"), 0, KEY_READ, &k) == ERROR_SUCCESS) {
        r = RegQueryInfoKey(k, classname, &classnamesize, NULL, &subkeys, &maxsubkey, &maxclass, &nvalues, &maxvalue, &maxvaluedata, &secdescr, &lastwrite);
        printf("RegQueryInfoKey = %d\n", r);
        if (nvalues) {
            printf("%d\n", nvalues);
            for (i = 0; i < nvalues; i++) {
                name_len = BLEN, data_len = BLEN;
                r = RegEnumValue(k, i, name, &name_len, NULL, &type, data, &data_len);
                if (r == ERROR_SUCCESS) {
                    printf("(%d) %ls: %ls\n", i, name, data);
                }
            }
        }
    } else {
        printf("listserial: RegOpenKeyEx failed\n");
    }

}
