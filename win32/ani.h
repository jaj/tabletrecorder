#ifndef ANI_H
#define ANI_H

#include <queue>
#include <mutex>
#include <thread>
#include <chrono>
#include <ctime>
#include <stdint.h>

#define F_MSGSIZE 65535

struct ani_t {
    struct timeval time;
    uint8_t ani_time[20];
    unsigned int ani_running;
    unsigned int ani_short;
    unsigned int ani_long;
    unsigned long long ani_energy;
};

class Ani
{
public:
    Ani();
    ~Ani();
    int start();
    int stop();
    int get_data(struct ani_t *);
    bool isrunning();
private:
    int connect(wchar_t *portname);
    int scan();
    void run();
    void process_msg();
    void process_byte(uint8_t);

    std::thread *fthread;
    std::queue<uint8_t> *q;
    std::mutex *ani_mutex;
    std::queue<ani_t> *aniq;
    uint8_t rxbuf[F_MSGSIZE];
    size_t rxbufindex;
    //enum msg_t lastmsg[7];
    bool running;
    bool stopflag;
};

#endif // ANI_H

