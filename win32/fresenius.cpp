#include "fresenius.h"

#ifdef _WIN32
#include <windows.h>
#include <winreg.h>
#include <process.h>
#else
#include <sys/ioctl.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#endif


#include <sys/time.h>
#include <string.h>
#include <stdio.h>
#include <queue>

#define BLEN 4096

Fresenius::Fresenius()
{
    memset(&state, 0, sizeof(state));
    memset(lastmsg, 0, sizeof(lastmsg));
    q = new std::queue<uint8_t>;
    vol_mutex = new std::mutex;
    vol = new std::queue<vol_t>;
    send_dc4 = false;
    send_ack = false;
    wait_ack = false;
    running = false;
}

Fresenius::~Fresenius()
{
    delete q;
    delete vol_mutex;
    delete vol;
}

void Fresenius::send(int addr, uint8_t *msg, size_t msglen)
{
    size_t i;
    uint8_t chksum;

    q->push(0x02);
    q->push(addr + '0');
    chksum = addr + '0';
    for (i = 0; i < msglen; i++) {
        q->push(msg[i]);
        chksum += msg[i];
    }
    chksum = ~chksum;
    printf("[<] Fresenius send %d%.*s%02X\n", addr, msglen, msg, chksum);
    q->push((chksum >> 4) > 9 ? (chksum >> 4) - 0xa + 'A' : (chksum >> 4) + '0');
    q->push((chksum & 0xf) > 9 ? (chksum & 0xf) - 0xa + 'A' : (chksum & 0xf) + '0');
    q->push(0x03);

    if (msg[0] != 'E')
        lastmsg[addr] = M_DC;
}

void Fresenius::process_msg()
{
    uint8_t src_addr;
    uint8_t syrstate;

    printf("[>] Fresenius recv %.*s\n", rxbufindex - 2, rxbuf);

    src_addr = rxbuf[0] - '0';
    send_ack = true;
    // check addr
    // check checksum
    // extract features
    // no shotgun parser maybe?
    // do not rely on pre-processing! assume random incoherent buffer!
    if (rxbufindex > 2 && rxbuf[1] == 'E') {
        // spontaneous message, send reception report
        send(rxbuf[0] - '0', (uint8_t *)"E", 1);
    }
    if (rxbuf[1] == 'C') {
        lastmsg[src_addr] = M_NONE;
    }
    if (rxbufindex > 2 && !memcmp(rxbuf, "0C;", 3) && !state[0]) {
        running = true;
        state[0] = true;
        send(0, (uint8_t *) "DE;b", 4);
    } else if (rxbufindex == 8 && !memcmp(rxbuf, "0E;b", 4)) {
        // spontaneous list of syringes from base, connected to them all
        syrstate = rxbuf[4] > '9' ? (rxbuf[4] - 'A') << 4 : (rxbuf[4] - '0') << 4;
        syrstate |= rxbuf[5] > '9' ? rxbuf[5] - 'A' : rxbuf[5] - '0';
        for (unsigned int i = 0; i < 6; ++i) {
            if (((syrstate >> i) & 1) != state[i+1]) {
                if (!state[i+1])
                    send(i + 1, (uint8_t *) "DC", 2);
                else {
                    state[i+1] = 0;
                }
            }
        }
    } else if (!memcmp(rxbuf + 1, "C;", 2) && !state[src_addr]) {
        send(src_addr, (uint8_t *) "DE;r", 4);
        state[src_addr] = 1;
    } else if (!memcmp(rxbuf + 1, "E;r", 3)) {
        rxbuf[rxbufindex - 2] = 0;
        struct vol_t v;
        //v.time = std::time(nullptr);
        struct timeval tv;
        gettimeofday(&tv, NULL);
        memcpy(&v.time, (uint8_t *) &tv, sizeof(struct timeval));
        v.syringe = src_addr;
        v.volume = strtoll((char *) rxbuf + 4, NULL, 16);
        vol_mutex->lock();
        vol->push(v);
        vol_mutex->unlock();
    }
    rxbufindex = 0;
    // wipe buffer
}

void Fresenius::process_byte(uint8_t c)
{
    printf("[>] Fresenius rx %c 0x%02x\n", c, c);

    if (c == 0x05)
        send_dc4 = true;
    else if (c == 0x02)
        rxbufindex = 0;
    else if (c == 0x03)
        process_msg();
    else if (c == 0x06) {
        wait_ack = false;
    } else {
        if (rxbufindex > 512 || rxbufindex < 0) {
            printf("???\n");
            rxbufindex = 0;
            stopflag = 1;
        } else {
            rxbuf[rxbufindex++] = c;
        }
    }
}

#ifdef _WIN32
int Fresenius::connect(wchar_t *portname)
{
    int r = -1;
    HANDLE hComm = NULL, hEvents[2];
    DCB old_dcb, new_dcb;
    COMMTIMEOUTS old_timeout, new_timeout;
    OVERLAPPED owrite, oread;
    DWORD rwait, len;
    BYTE rbuf = 0, wbuf = 0;
    BOOL dcb_saved = FALSE, timeout_saved = FALSE, writing = FALSE, reading = FALSE;

    FillMemory(&state, sizeof(state), 0);
    FillMemory(lastmsg, sizeof(lastmsg), 0);
    send_dc4 = false;
    send_ack = false;
    wait_ack = false;
    rxbufindex = 0;

    if (portname == NULL) {
        printf("[!] Fresenius NULL is an interesting port name\n");
        goto connect_cleanup;
    }

    // Open port
    hComm = CreateFile((LPCWSTR) portname, GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, FILE_FLAG_OVERLAPPED, 0);
    if (hComm == INVALID_HANDLE_VALUE) {
        printf("[!] Fresenius CreateFile() failed: 0x%lx\n", GetLastError());
        goto create_cleanup;
    }

    // Save and set device-control block
    FillMemory(&old_dcb, sizeof(old_dcb), 0);
    if (!GetCommState(hComm, &old_dcb)) {
        printf("[!] Fresenius GetCommState() failed: 0x%lx\n", GetLastError());
        goto connect_cleanup;
    }
    dcb_saved = TRUE;
    FillMemory(&new_dcb, sizeof(new_dcb), 0);
    if (!BuildCommDCB(TEXT("19200,e,7,1"), &new_dcb)) {
        printf("[!] Fresenius BuildCommDCB() failed: 0x%lx\n", GetLastError());
        goto connect_cleanup;
    }
    if (!SetCommState(hComm, &new_dcb)) {
        printf("[!] Fresenius SetCommState() failed: 0x%lx\n", GetLastError());
        goto connect_cleanup;
    }

    // Save and set timeouts
    FillMemory(&old_timeout, sizeof(old_timeout), 0);
    if (!SetCommTimeouts(hComm, &new_timeout)) {
        printf("[!] Fresenius GetCommState() failed: 0x%lx\n", GetLastError());
        goto connect_cleanup;
    }
    timeout_saved = TRUE;
    new_timeout.ReadIntervalTimeout = 0;
    new_timeout.ReadTotalTimeoutMultiplier = 0;
    new_timeout.ReadTotalTimeoutConstant = 1000;
    new_timeout.WriteTotalTimeoutMultiplier = 10;
    new_timeout.WriteTotalTimeoutConstant = 100;
    if (!SetCommTimeouts(hComm, &new_timeout)) {
        printf("[!] Fresenius GetCommState() failed: 0x%lx\n", GetLastError());
        goto connect_cleanup;
    }

    // Prepare events
    hEvents[0] = CreateEvent(NULL, TRUE, FALSE, NULL);
    hEvents[1] = CreateEvent(NULL, TRUE, FALSE, NULL);
    if (hEvents[0] == NULL || hEvents[1] == NULL) {
        printf("[!] Fresenius CreateEvent() failed: 0x%lx\n", GetLastError());
        goto connect_cleanup;
    }
    FillMemory(&oread, sizeof(oread), 0);
    FillMemory(&owrite, sizeof(owrite), 0);
    oread.hEvent = hEvents[0];
    owrite.hEvent = hEvents[1];

    // queue a connection request to the base
    send(0, (uint8_t *) "DC", 2);

    for (;;) {
        if (!writing && ((!q->empty() && !wait_ack) || send_dc4 || send_ack)) {
            if (send_dc4) {
                wbuf = 0x14;
                send_dc4 = false;
            } else if (send_ack) {
                wbuf = 0x06;
                send_ack = false;
            } else {
                wbuf = q->front();
                q->pop();
                if (wbuf == 0x03)
                    wait_ack = true;
            }
            //printf("[<] sending %02x %c\n", wbuf, wbuf);
            if (!WriteFile(hComm, &wbuf, 1, NULL, &owrite)) {
                if (GetLastError() != ERROR_IO_PENDING) {
                    printf("[!] Fresenius WriteFile: 0x%lx\n", GetLastError());
                    goto connect_cleanup;
                }
                writing = TRUE;
            } else {
                printf("[+] Fresenius WriteFile() returned immediately!\n");
                continue;
            }
        }
        if (!reading) {
            if (!ReadFile(hComm, &rbuf, 1, &len, &oread)) {
                if (GetLastError() != ERROR_IO_PENDING) {
                    printf("[!] Fresenius ReadFile() failed with error %lx\n", GetLastError());
                    goto connect_cleanup;
                }
                reading = TRUE;
            } else {
                //printf("[+] Fresenius ReadFile() returned immediately!\n");
                continue;
            }
        }
        rwait = WaitForMultipleObjects(2, hEvents, FALSE, 2000);
        switch (rwait) {
            case WAIT_OBJECT_0: // read
                if (!GetOverlappedResult(hComm, &oread, &len, FALSE)) {
                    printf("[!] Fresenius GetOverlappedResult() failed\n");
                    goto connect_cleanup;
                }
                if (len == 0) {
                    printf("[!] Fresenius timeout reading\n");
                    goto connect_cleanup;
                }
                //printf("[>] read %02x\n", rbuf);
                process_byte(rbuf);
                reading = FALSE;
                ResetEvent(hEvents[0]);
                break;
            case WAIT_OBJECT_0 + 1: // write
                if (!GetOverlappedResult(hComm, &owrite, &len, FALSE)) {
                    printf("[!] Fresenius GetOverlappedResult() failed\n");
                    goto connect_cleanup;
                }
                writing = FALSE;
                ResetEvent(hEvents[1]);
                break;
            case WAIT_TIMEOUT:
                printf("[!] Fresenius WaitFormultipleObject() timeout!\n");
            default:
                printf("[!] Fresenius WaitForMultipleObject() failed\n");
                goto connect_cleanup;
        }
        fflush(stdout);
        if (stopflag)
            goto connect_cleanup;
    }

connect_cleanup:
    printf("[-] Fresenius cleaning up %ls\n", portname);
    // disconnect nicely?
    if (hEvents[0] != NULL && hEvents[0] != INVALID_HANDLE_VALUE)
        if (!CloseHandle(hEvents[0]))
            printf("[!] Fresenius failed to close handle: 0x%lx\n", GetLastError());
    if (hEvents[1] != NULL && hEvents[1] != INVALID_HANDLE_VALUE)
        if (!CloseHandle(hEvents[1]))
            printf("[!] Fresenius failed to close handle: 0x%lx\n", GetLastError());
    if (timeout_saved)
        if (!SetCommTimeouts(hComm, &old_timeout))
            printf("[!] Fresenius failed to restore timeouts: 0x%lx\n", GetLastError());
    if (dcb_saved)
        if (!SetCommState(hComm, &old_dcb))
            printf("[!] Fresenius failed to restore DCB: 0x%lx\n", GetLastError());
    if (hComm != NULL && hComm != INVALID_HANDLE_VALUE)
        if (!CloseHandle(hComm))
            printf("[!] Fresenius failed to close handle: 0x%lx\n", GetLastError());
    fflush(stdout);
create_cleanup:
    running = false;
    return r;
}
#else
int Fresenius::connect(char *portname)
{
    int r = -1;
    int fd;
    int rwait, len;
    uint8_t rbuf = 0, wbuf = 0;
    bool writing = false, reading = false;

    memset(&state, 0, sizeof(state));
    memset(lastmsg, 0, sizeof(lastmsg));
    send_dc4 = false;
    send_ack = false;
    wait_ack = false;
    rxbufindex = 0;

    if (portname == NULL) {
        printf("[!] Fresenius NULL is an interesting port name\n");
        goto connect_cleanup;
    }

    // Open port
    fd = open(portname, O_RDWR | O_NOCTTY | O_NDELAY);
    if (fd == -1) {
        printf("[!] Fresenius open() failed\n");
        goto create_cleanup;
    }

    tcgetattr(fd, &sock_opt);
    cfmakeraw(&sock_opt);
    cfsetispeed(&sock_opt, B19200);
    cfsetospeed(&sock_opt, B19200);
    // 7-bit data
    sock_opt.c_cflag = (sock_opt.c_cflag & ~CSIZE) | CS7;
    // even parity
    sock_opt.c_cflag &= ~(PARENB | PARODD);
    sock_opt.c_cflag |= PARENB;
    // 1 stop bit
    sock_opt.c_cflag &= ~CSTOPB;
    // raw output
    sock_opt.c_oflag = 0;
    // clear hardware flow control
    sock_opt.c_cflag &= ~CRTSCTS;

    sock_opt.c_cflag |= (CLOCAL | CREAD);

    // clean up local mode flag
    //sock_opt.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);

    // timeouts
    sock_opt.c_cc[VMIN] = 0;
    sock_opt.c_cc[VTIME] = 10;

    tcsetattr(fd, TCSANOW, &sock_opt);
    tcsetattr(fd, TCSAFLUSH, &sock_opt);
    ioctl(fd, TIOCMGET, &comm_bits);
    comm_bits |= TIOCM_DTR | TIOCM_RTS;
    ioctl(fd, TIOCMSET, &comm_bits);

    // queue a connection request to the base
    send(0, (uint8_t *) "DC", 2);

    for (;;) {
        if (!writing && ((!q->empty() && !wait_ack) || send_dc4 || send_ack)) {
            if (send_dc4) {
                wbuf = 0x14;
                send_dc4 = false;
            } else if (send_ack) {
                wbuf = 0x06;
                send_ack = false;
            } else {
                wbuf = q->front();
                q->pop();
                if (wbuf == 0x03)
                    wait_ack = true;
            }
            //printf("[<] Fresenius sending %02x %c\n", wbuf, wbuf);
            if (!WriteFile(hComm, &wbuf, 1, NULL, &owrite)) {
                if (GetLastError() != ERROR_IO_PENDING) {
                    printf("[!] Fresenius WriteFile: 0x%lx\n", GetLastError());
                    goto connect_cleanup;
                }
                writing = true;
            } else {
                printf("[+] Fresenius write() returned immediately!\n");
                continue;
            }
        }
        if (!reading) {
            if ()) {
                if (GetLastError() != ERROR_IO_PENDING) {
                    printf("[!] Fresenius read() failed with error %lx\n", GetLastError());
                    goto connect_cleanup;
                }
                reading = true;
            } else {
                //printf("[+] Fresenius read() returned immediately!\n");
                continue;
            }
        }
        rwait = WaitForMultipleObjects(2, hEvents, FALSE, 2000);
        switch (rwait) {
            case WAIT_OBJECT_0: // read
                if (!GetOverlappedResult(hComm, &oread, &len, FALSE)) {
                    printf("[!] Fresenius GetOverlappedResult() failed\n");
                    goto connect_cleanup;
                }
                if (len == 0) {
                    printf("[!] Fresenius timeout reading\n");
                    goto connect_cleanup;
                }
                //printf("[>] Fresenius read %02x\n", rbuf);
                process_byte(rbuf);
                reading = false;
                ResetEvent(hEvents[0]);
                break;
            case WAIT_OBJECT_0 + 1: // write
                if (!GetOverlappedResult(hComm, &owrite, &len, FALSE)) {
                    printf("[!] Fresenius GetOverlappedResult() failed\n");
                    goto connect_cleanup;
                }
                writing = false;
                ResetEvent(hEvents[1]);
                break;
            case WAIT_TIMEOUT:
                printf("[!] Fresenius WaitFormultipleObject() timeout!\n");
            default:
                printf("[!] Fresenius WaitForMultipleObject() failed\n");
                goto connect_cleanup;
        }
        fflush(stdout);
        if (stopflag)
            goto connect_cleanup;
    }

connect_cleanup:
    printf("[-] Fresenius cleaning up %ls\n", portname);
create_cleanup:
    tcflush(fd, TCIOFLUSH);
    close(fd);
    fflush(stdout);
    running = false;
    return r;
}
#endif

#ifdef _WIN32
int Fresenius::scan()
{
    HKEY k;
    DWORD i, cnamelen = MAX_PATH, subkeys = 0, maxsubkey, maxclass, nvalues, maxvalue, maxvaluedata, secdescr, regnamelen, datalen, type;
    TCHAR cname[MAX_PATH], regname[BLEN];
    BYTE data[BLEN];
    FILETIME lastwrite;

    if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, TEXT("HARDWARE\\DEVICEMAP\\SERIALCOMM"), 0, KEY_READ, &k) == ERROR_SUCCESS) {
        if (RegQueryInfoKey(k, cname, &cnamelen, NULL, &subkeys, &maxsubkey, &maxclass, &nvalues, &maxvalue, &maxvaluedata, &secdescr, &lastwrite) == ERROR_SUCCESS) {
            for (i = 0; i < nvalues; i++) {
                regnamelen = BLEN, datalen = BLEN;
                if (RegEnumValue(k, i, regname, &regnamelen, NULL, &type, data, &datalen) == ERROR_SUCCESS) {
                    if (type == REG_SZ) {
                        printf("[+] Fresenius connecting to serial port %ls (%lu/%lu)\n", (wchar_t *) data, i + 1, nvalues);
                        fflush(stdout);
                        connect((wchar_t *) data);
                    }
                }
            }
        }
    }
    printf("[?] Fresenius no fresenius device found\n");
    fflush(stdout);
    return -1;
}
#else
int Fresenius::scan()
{
    char dev[4096];
    DIR *d;
    struct dirent *dir;
    
    d = opendir("/dev/serial/by-id");
    if (d) {
        while ((dir = readdir(d)) != NULL) {
            if (strlen(dir->d_name) > 3) {
                memset(dev, 0, 4096);
                strcat(dev, "/dev/serial/by-id/");
                strncat(dev, dir->d_name, 2048);
                printf("[+] Fresenius connecting to serial port %s\n", dev);
                fflush(stdout);
                connect(dev);
            }
        }
        closedir(d);
    }
    printf("[?] Fresenius no fresenius device found\n");
    fflush(stdout);
    return -1;
}
#endif

void Fresenius::run()
{
    stopflag = false;
    for (;;) {
        scan();
        if (stopflag)
            return;
#ifdef _WIN
        Sleep(3000);
#else
        sleep(3000);
#endif
        if (stopflag)
            return;
    }
}

int Fresenius::start()
{
    printf("[+] Fresenius starting thread\n");
    fflush(stdout);

    fthread = new std::thread(&Fresenius::run, this);

    return 0;
}

int Fresenius::stop()
{
    printf("[+] Fresenius waiting for the thread to stop\n");
    fflush(stdout);
    stopflag = 1;
    fthread->join();
    delete fthread;
    return 0;
}

bool Fresenius::isrunning()
{
    return running;
}

int Fresenius::get_volume(struct vol_t *v)
{
    if (vol->empty())
        return -1;
    v->time = vol->front().time;
    v->syringe = vol->front().syringe;
    v->volume = vol->front().volume;
    vol_mutex->lock();
    vol->pop();
    vol_mutex->unlock();
    return 0;
}

