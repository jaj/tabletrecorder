#include "ani.h"

#include <windows.h>
#include <winreg.h>
#include <process.h>

#include <sys/time.h>
#include <stdio.h>
#include <queue>
#include <string.h>
#include <stdlib.h>

#define BLEN 65535

Ani::Ani()
{
    q = new std::queue<uint8_t>;
    ani_mutex = new std::mutex;
    aniq = new std::queue<ani_t>;
    rxbufindex = 0;
    running = false;
    memset(rxbuf, 0, 65535);
}

Ani::~Ani()
{
    delete q;
    delete ani_mutex;
    delete aniq;
}

void Ani::process_msg()
{   
    char *s, *t, *ptr;
    struct ani_t v;
    struct timeval tv;
    
    if (strlen((char*)rxbuf) < 32)
        return;

    //printf("[>] ANI recv ( %s )\n", rxbuf);
    //fflush(stdout);
    memset(&v, 0, sizeof(struct ani_t));
    gettimeofday(&tv, NULL);
    memcpy(&v.time, (uint8_t *) &tv, sizeof(struct timeval));
    memcpy(v.ani_time, rxbuf, 19);
    v.ani_time[0] = rxbuf[3];
    v.ani_time[1] = rxbuf[4];
    v.ani_time[2] = '-';
    v.ani_time[3] = rxbuf[0];
    v.ani_time[4] = rxbuf[1];
    v.ani_time[5] = '-';
    v.ani_time[10] = ' ';
    v.ani_running = rxbuf[20] - '0';
    if (v.ani_running) {
        v.ani_short = strtoll((char*)rxbuf + 22, NULL, 10);
        v.ani_long = strtoll((char*)rxbuf + 26, NULL, 10);
        v.ani_energy = (long long) (strtod((char*)rxbuf + 30, NULL) * 1000);
    }
    ani_mutex->lock();
    aniq->push(v);
    ani_mutex->unlock();
}

void Ani::process_byte(uint8_t c)
{
    if (rxbufindex > 3 && c == 0x0a && rxbuf[rxbufindex-1] == 0x0d) {
        rxbuf[rxbufindex-1] = 0;
        process_msg();
        rxbufindex = 0;
        memset(rxbuf, 0, 65535);
    } else {
        if (rxbufindex > 4096) {
            rxbufindex = 0;
            memset(rxbuf, 0, 65535);
        }
        rxbuf[rxbufindex++] = c;
    }
}

int Ani::connect(wchar_t *portname)
{
    int r = -1;
    HANDLE hComm = NULL;
    DCB old_dcb, new_dcb;
    COMMTIMEOUTS old_timeout, new_timeout;
    DWORD rwait, len;
    BYTE rbuf;
    BOOL dcb_saved = FALSE, timeout_saved = FALSE;

    if (portname == NULL) {
        printf("[!] ANI NULL is an interesting port name\n");
        goto connect_cleanup;
    }

    // Open port
    hComm = CreateFile((LPCWSTR) portname, GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, 0, 0);
    if (hComm == INVALID_HANDLE_VALUE) {
        printf("[!] ANI CreateFile() failed: 0x%lx\n", GetLastError());
        goto create_cleanup;
    }

    // Save and set device-control block
    FillMemory(&old_dcb, sizeof(old_dcb), 0);
    if (!GetCommState(hComm, &old_dcb)) {
        printf("[!] ANI GetCommState() failed: 0x%lx\n", GetLastError());
        goto connect_cleanup;
    }
    dcb_saved = TRUE;
    FillMemory(&new_dcb, sizeof(new_dcb), 0);
    if (!BuildCommDCB(TEXT("9600,n,8,1"), &new_dcb)) {
        printf("[!] ANI BuildCommDCB() failed: 0x%lx\n", GetLastError());
        goto connect_cleanup;
    }
    if (!SetCommState(hComm, &new_dcb)) {
        printf("[!] ANI SetCommState() failed: 0x%lx\n", GetLastError());
        goto connect_cleanup;
    }

    // Save and set timeouts
    FillMemory(&old_timeout, sizeof(old_timeout), 0);
    if (!SetCommTimeouts(hComm, &new_timeout)) {
        printf("[!] ANI GetCommState() failed: 0x%lx\n", GetLastError());
        goto connect_cleanup;
    }
    timeout_saved = TRUE;
    new_timeout.ReadIntervalTimeout = 0;
    new_timeout.ReadTotalTimeoutMultiplier = 0;
    new_timeout.ReadTotalTimeoutConstant = 5000;
    new_timeout.WriteTotalTimeoutMultiplier = 10;
    new_timeout.WriteTotalTimeoutConstant = 100;
    if (!SetCommTimeouts(hComm, &new_timeout)) {
        printf("[!] ANI GetCommState() failed: 0x%lx\n", GetLastError());
        goto connect_cleanup;
    }

    memset(rxbuf, 0, 65535);

    for (;;) {
        if (!ReadFile(hComm, &rbuf, 1, &len, 0)) {
            if (GetLastError() != ERROR_IO_PENDING) {
                printf("[!] ANI ReadFile() failed with error %lx\n", GetLastError());
                goto connect_cleanup;
            }
        } else {
            process_byte(rbuf);
            if (len == 0) {
                break;
            }
        }
        if (stopflag) {
            goto connect_cleanup;
        }
    }

connect_cleanup:
    printf("[-] ANI cleaning up port %ls\n", portname);
    if (timeout_saved)
        if (!SetCommTimeouts(hComm, &old_timeout))
            printf("[!] ANI failed to restore timeouts: 0x%lx\n", GetLastError());
    if (dcb_saved)
        if (!SetCommState(hComm, &old_dcb))
            printf("[!] ANI failed to restore DCB: 0x%lx\n", GetLastError());
    if (hComm != NULL && hComm != INVALID_HANDLE_VALUE)
        if (!CloseHandle(hComm))
            printf("[!] ANI failed to close handle: 0x%lx\n", GetLastError());
    fflush(stdout);
create_cleanup:
    running = false;
    return r;
}

int Ani::scan()
{
    HKEY k;
    DWORD i, cnamelen = MAX_PATH, subkeys = 0, maxsubkey, maxclass, nvalues, maxvalue, maxvaluedata, secdescr, regnamelen, datalen, type;
    TCHAR cname[MAX_PATH], regname[BLEN];
    BYTE data[BLEN];
    FILETIME lastwrite;

    if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, TEXT("HARDWARE\\DEVICEMAP\\SERIALCOMM"), 0, KEY_READ, &k) == ERROR_SUCCESS) {
        if (RegQueryInfoKey(k, cname, &cnamelen, NULL, &subkeys, &maxsubkey, &maxclass, &nvalues, &maxvalue, &maxvaluedata, &secdescr, &lastwrite) == ERROR_SUCCESS) {
            for (i = 0; i < nvalues; i++) {
                regnamelen = BLEN, datalen = BLEN;
                if (RegEnumValue(k, i, regname, &regnamelen, NULL, &type, data, &datalen) == ERROR_SUCCESS) {
                    if (type == REG_SZ) {
                        printf("[+] ANI connecting to serial port %ls (%lu/%lu)\n", (wchar_t *) data, i + 1, nvalues);
                        fflush(stdout);
                        connect((wchar_t *) data);
                    }
                }
            }
        }
    }
    printf("[?] ANI no ani device found\n");
    fflush(stdout);
    return -1;
}

void Ani::run()
{
    stopflag = false;
    for (;;) {
        scan();
        if (stopflag)
            return;
        Sleep(5000);
        if (stopflag)
            return;
    }
}

int Ani::start()
{
    printf("[+] ANI starting thread\n");
    fflush(stdout);

    fthread = new std::thread(&Ani::run, this);

    return 0;
}

int Ani::stop()
{
    printf("[+] ANI waiting for the thread to stop\n");
    fflush(stdout);

    stopflag = 1;
    fthread->join();
    delete fthread;
    return 0;
}

bool Ani::isrunning()
{
    return running;
}

int Ani::get_data(struct ani_t *a)
{
    if (aniq->empty())
        return -1;
    a->time = aniq->front().time;
    memcpy(a->ani_time, aniq->front().ani_time, 20);
    a->ani_running = aniq->front().ani_running;
    a->ani_short = aniq->front().ani_short;
    a->ani_long = aniq->front().ani_long;
    a->ani_energy = aniq->front().ani_energy;
    ani_mutex->lock();
    aniq->pop();
    ani_mutex->unlock();
    return 0;
}

