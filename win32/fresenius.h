#ifndef FRESENIUS_H
#define FRESENIUS_H

#include <queue>
#include <mutex>
#include <thread>
#include <chrono>
#include <ctime>
#include <stdint.h>

#define F_MSGSIZE 256

struct vol_t {
    struct timeval time;
    unsigned int syringe;
    unsigned long long volume;
};

enum msg_t {
    M_NONE, M_DC, M_FC, M_MO,
    M_RZ, M_OF, M_SI, M_EP,
    M_LP, M_AP, M_DE, M_AE,
    M_LE, M_DM, M_AM, M_LM,
    M_EN, M_LN, M_LF, M_PR,
    M_PO, M_PB, M_PF, M_PV,
    M_RV, M_PP, M_PS
};

class Fresenius
{
public:
    Fresenius();
    ~Fresenius();
    int start();
    int stop();
    int get_volume(struct vol_t *);
    bool isrunning();
private:
#ifdef _WIN32
    int connect(wchar_t *portname);
#else
    int connect(char *portname);
#endif
    void send(int addr, uint8_t *msg, size_t msglen);
    int scan();
    void run();
    void process_msg();
    void process_byte(uint8_t);

    std::thread *fthread;
    std::queue<uint8_t> *q;
    uint8_t state[7];
    std::mutex *vol_mutex;
    std::queue<vol_t> *vol;
    uint8_t rxbuf[F_MSGSIZE];
    size_t rxbufindex;
    enum msg_t lastmsg[7];
    bool running;
    bool stopflag;
    bool send_dc4;
    bool send_ack;
    bool wait_ack;
};

#endif // FRESENIUS_H

