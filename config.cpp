#include "config.h"

Config::Config()
{
    QString confPath = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    if (confPath.isEmpty()) {
        qDebug() << "No config path";
        return;
    }
    QDir confDir(confPath);
    if (!confDir.exists())
        confDir.mkpath(confPath);

    QString eventPath = confDir.filePath("events.txt");
    eventFile = new QFile(eventPath);
    if (!eventFile->exists())
        saveDefaultEvents();
}

int Config::loadEvents(QStringList *eventList)
{
    char buf[4096];

    if (eventFile->open(QIODevice::ReadOnly)) {
        qint64 lineLen;
        while ((lineLen = eventFile->readLine(buf, sizeof(buf))) != -1) {
            if (lineLen > 1) {
                for (int i = 0; i < lineLen; i++)
                    if (buf[i] == '\n' || buf[i] == '\r')
                        buf[i] = 0;
                eventList->append(buf);
            }
        }
        eventFile->close();
    }
    return 0;
}

int Config::saveEvents(QStringList *eventList)
{
    if (eventFile->open(QIODevice::WriteOnly | QIODevice::Truncate)) {
        foreach (QString s, *eventList) {
            eventFile->write(s.toUtf8());
        }
        eventFile->close();
    }
    return 0;
}

int Config::saveDefaultEvents()
{
    if (eventFile->open(QIODevice::WriteOnly | QIODevice::Truncate)) {
        eventFile->write("Oxygenation\nTracrium\nCelocurine\nLaryngoscopie Intubation\nVentilation Mecanique\nKetamine\nSufentanyl\nBolus Propofol IV\nRemplissage\nExtubation\nBolus Peridural\n");
        eventFile->close();
    }
    return 0;
}

